//
//  RegisterViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/15/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class RegisterViewController: UIViewController {

    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    var ref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerAccount(sender: AnyObject) {
        if self.emailField.text == "" || self.passwordField.text == "" {
            let alertController = UIAlertController(title: "Oops!", message: "Please enter your email and password.", preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else {
            FIRAuth.auth()?.createUserWithEmail(self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                if error == nil
                {
                    let name = self.usernameField.text!
                    let email = self.emailField.text!
                    self.ref.child("users").child("\(user!.uid)/name").setValue(name)
                    self.ref.child("users").child("\(user!.uid)/email").setValue(email)
                    self.ref.child("users").child("\(user!.uid)/uid").setValue((user!.uid))
                    
                    let mapvc = MapsViewController(nibName: "MapsViewController", bundle: nil)
                    self.navigationController?.pushViewController(mapvc, animated: true)
                    
                } else {
                    let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
        }
        
    }
    

}
