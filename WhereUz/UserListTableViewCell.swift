//
//  UserListTableViewCell.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/19/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase

class UserListTableViewCell: UITableViewCell {
    
    var ref = FIRDatabase.database().reference()
    var user = FIRAuth.auth()?.currentUser
    var users = [User]()
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    weak var userdata: User? {
        didSet {
            
            nameLabel.text = userdata?.name
           
            ref.child("following").child(FIRAuth.auth()!.currentUser!.uid).observeEventType(.Value, withBlock: { (snapshot) in
                
                if snapshot.hasChild("\(self.userdata!.uid!)") {
                    self.followButton.setTitle("Following", forState: .Normal)
                }                
                }, withCancelBlock: nil )
            
        }
    }
    
    
    @IBAction func followAction(sender: AnyObject) {
        self.ref.child("following").child("\(user!.uid)/\(userdata!.uid!)").setValue(true)
        self.ref.child("followed").child("\(userdata!.uid!)/\(user!.uid)").setValue(true)
        
        followButton.setTitle("Following", forState: .Normal)
        
    }
}