//
//  UserLoginViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/14/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit

class UserLoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailLoginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var emailLogoutButton: UIButton!
    @IBOutlet weak var seemapsButton: UIButton!
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookLoginButton.delegate = self
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        if (FIRAuth.auth()?.currentUser) != nil
        {
            FIRDatabase.database().reference().child("users").child(FIRAuth.auth()!.currentUser!.uid).observeEventType(.Value, withBlock: { (snapshot) in
                
                if let usersDict = snapshot.value as? [String: AnyObject] {
                    let userr = User()
                    userr.setValuesForKeysWithDictionary(usersDict)
                    
                    dispatch_async(dispatch_get_main_queue(), {
                        self.usernameLabel.text = "Welcome, \(userr.name!)! ^^" })
                    
                }
                }, withCancelBlock: nil)
                
                self.emailLogoutButton.alpha = 1.0
                self.seemapsButton.alpha = 1.0
                self.emailLoginButton.alpha = 0.0
                self.registerButton.alpha = 0.0
                self.emailField.alpha = 0.0
                self.passwordField.alpha=0.0
//                self.usernameLabel.text = "Welcome \(user.email!)"
               
        }
            
                else {
                self.emailLogoutButton.alpha = 0.0
                self.seemapsButton.alpha = 0.0
                self.emailLoginButton.alpha = 1.0
                self.registerButton.alpha = 1.0
                self.emailField.alpha = 1.0
                self.passwordField.alpha=1.0
                self.usernameLabel.text = "Please register or login here"
            }
        }
        
        
        
        @IBAction func registerAction(sender: AnyObject) {
            let registervc = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
            self.navigationController?.pushViewController(registervc, animated: true)
        }
        
        @IBAction func seemapsAction(sender: AnyObject) {
            let mapsvc = MapsViewController(nibName: "MapsViewController", bundle: nil)
            self.navigationController?.pushViewController(mapsvc, animated: true)
        }
        
        
        @IBAction func loginAction(sender: AnyObject) {
            if self.emailField.text == "" || self.passwordField.text == "" {
                let alertController = UIAlertController(title: "Oops!", message: "Please enter your email and password.", preferredStyle: .Alert)
                let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                FIRAuth.auth()?.signInWithEmail(self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                    if error == nil
                    {
                        // self.logoutButton.alpha = 1.0
                        //self.usernameLabel.text = user!.email
                        //self.emailField.text = ""
                        //self.passwordField.text = ""
                        let mapvc = MapsViewController(nibName: "MapsViewController", bundle: nil)
                        self.navigationController?.pushViewController(mapvc, animated: true)
                        
                    } else {
                        let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .Alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                    }
                })
            }
            
        }
        
        @IBAction func emailLogoutAction(sender: AnyObject) {
            try! FIRAuth.auth()?.signOut()
            
            self.emailLogoutButton.alpha = 0.0
            self.emailLoginButton.alpha = 1.0
            self.registerButton.alpha = 1.0
            self.emailField.alpha = 1.0
            self.passwordField.alpha=1.0
            self.usernameLabel.text = "Please register or login here"
            self.emailField.text = ""
            self.passwordField.text = ""
            
            let loginvc = UserLoginViewController(nibName: "UserLoginViewController", bundle: nil)
            //        self.presentViewController(loginvc, animated: true, completion: nil)
            self.navigationController?.pushViewController(loginvc, animated: true)
        }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            let credential = FIRFacebookAuthProvider.credentialWithAccessToken(FBSDKAccessToken.currentAccessToken().tokenString)
            FIRAuth.auth()?.signInWithCredential(credential, completion: { (user, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    return
                }
                print("User logged in with facebook ...")
                
                let vc = MapsViewController(nibName: "MapsViewController", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            })
            
            
            /*
             // MARK: - Navigation
             
             // In a storyboard-based application, you will often want to do a little preparation before navigation
             override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
             // Get the new view controller using segue.destinationViewController.
             // Pass the selected object to the new view controller.
             }
             */
            
        }
        
        func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
            try! FIRAuth.auth()!.signOut()
            print("User logged out from facebook ...")
        }
}