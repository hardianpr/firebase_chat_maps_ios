//
//  FriendTableViewCell.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/20/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase

class FriendTableViewCell: UITableViewCell {

    weak var userdata: User? {
        didSet {
            textLabel?.text = userdata?.name
    
}
}
}