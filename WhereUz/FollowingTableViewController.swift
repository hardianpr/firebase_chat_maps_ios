//
//  FollowingTableViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/21/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase

class FollowingTableViewController: UITableViewController {
        
    let cellId = "cellId"
    
    var users = [User]()
    
    var user = FIRAuth.auth()?.currentUser
    
    var ref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "FollowingTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellId)
        
        fetchFriend()
    }
    
    @IBAction func findPeopleAction(sender: AnyObject) {
        let uservc = UserListTableViewController(nibName: "UserListTableViewController", bundle: nil)
        self.navigationController?.pushViewController(uservc, animated: true)
    }
    
    func fetchFriend() {
        FIRDatabase.database().reference().child("following").child(FIRAuth.auth()!.currentUser!.uid).observeEventType(.Value, withBlock: { (snapshot) in

    
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                for (key, _) in dictionary {
              
                    FIRDatabase.database().reference().child("users").child("\(key)").observeEventType(.Value, withBlock: { (snapshot) in
                        
                        if let followingDictionary = snapshot.value as? [String: AnyObject] {
                            let user = User()
                            user.setValuesForKeysWithDictionary(followingDictionary)
                            self.users.append(user)
                            
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                self.tableView.reloadData()
                            })
                        }
                    
                }, withCancelBlock: nil)
                }
            }
            
            }, withCancelBlock: nil)
}

    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! FollowingTableViewCell
        
        let user = users[indexPath.row]
        
        cell.userdata = user
        
        return cell
    }
    
}
