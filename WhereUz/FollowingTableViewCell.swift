//
//  FollowingTableViewCell.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/21/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase

class FollowingTableViewCell: UITableViewCell {
    
    var ref = FIRDatabase.database().reference()
    var user = FIRAuth.auth()?.currentUser
    var users = [User]()

    weak var userdata: User? {
        didSet {
            textLabel?.text = userdata?.name
            textLabel?.textColor = UIColor.blueColor()
            
            ref.child("followed").child(FIRAuth.auth()!.currentUser!.uid).observeEventType(.Value, withBlock: { (snapshot) in
                
                if snapshot.hasChild("\(self.userdata!.uid!)") {
                    self.detailTextLabel?.text = "follows you"
                    self.detailTextLabel?.textColor = UIColor.brownColor()
                }
                else {
                    self.detailTextLabel?.text = ""
                }
                }, withCancelBlock: nil )

        }
    }
}
