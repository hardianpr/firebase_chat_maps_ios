//
//  UserListTableViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/15/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class UserListTableViewController: UITableViewController {
    
    let cellId = "cellId"
    
    var users = [User]()
    
    var user = FIRAuth.auth()?.currentUser
    
    var ref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(handleCancel))
        
        let nib = UINib(nibName: "UserListTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellId)
        
        fetchUser()
    }
    
    func fetchUser() {
        
        FIRDatabase.database().reference().child("users").observeEventType(.ChildAdded, withBlock: { (snapshot) in
            
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User()
                user.setValuesForKeysWithDictionary(dictionary)
                
                self.users.append(user)
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.tableView.reloadData()
                })
                
                print(snapshot.key)
                print(dictionary)
            }
    
            
            }, withCancelBlock: nil)
    }
    
    
    
    
    // MARK: - Table view data source
    
    /* override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }*/
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! UserListTableViewCell
        
        let user = users[indexPath.row]
        
        cell.userdata = user
        
        return cell
    }
}

/*class UserCell: UITableViewCell {
 override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
 super.init(style: .Subtitle, reuseIdentifier: reuseIdentifier)
 }
 
 required init?(coder aDecoder: NSCoder) {
 fatalError("init(coder:) has not been implemented")
 }
 }*/