//
//  MapsViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/14/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase
import FirebaseAuth

class MapsViewController: UIViewController {
    
    
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    var ref = FIRDatabase.database().reference()
    var user = FIRAuth.auth()?.currentUser
    var users = [User]()
    var mapAnnotations:[PinAnnotation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showsUserLocation = true
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        fetchLocation()
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func fetchLocation() {
        
        FIRDatabase.database().reference().child("following").child(FIRAuth.auth()!.currentUser!.uid).observeEventType(.Value, withBlock: { (snapshot) in
            
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                for (key, _) in dictionary {
                    
                    FIRDatabase.database().reference().child("users").child("\(key)").observeEventType(.Value, withBlock: { (snapshot) in

                        if let mapDictionary = snapshot.value as? [String: AnyObject] {
                            print(mapDictionary)
                            
                            let user = User()
                            user.setValuesForKeysWithDictionary(mapDictionary)
                            self.users.append(user)
                            
                            let lat = Double(user.latitude!)
                            let lon = Double(user.longitude!)
                            let title = user.name
                            let subTitle = "Hello there!"
                            
                            let annotation:PinAnnotation = PinAnnotation(coordinate: CLLocationCoordinate2DMake(lat!, lon!), title: title!, subtitle: subTitle)
                            self.mapAnnotations.append(annotation)
                            self.mapView.addAnnotations(self.mapAnnotations)
                            
                        }
                        
                        }, withCancelBlock: nil)
                }
            }
            
            }, withCancelBlock: nil)
        
    }
    
    
    @IBAction func emailLogoutAction(sender: AnyObject) {
        try! FIRAuth.auth()?.signOut()
        
        let loginvc = UserLoginViewController(nibName: "UserLoginViewController", bundle: nil)
//        self.navigationController?.presentViewController(loginvc, animated: true, completion: nil)
        
        self.navigationController?.pushViewController(loginvc, animated: true)
    }
    
    @IBAction func findFriendsButton(sender: AnyObject) {
        let ffvc = FollowingTableViewController(nibName: "FollowingTableViewController", bundle: nil)
        self.navigationController?.pushViewController(ffvc, animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MapsViewController : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.first
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let latitude = String(location!.coordinate.latitude)
        let longitude = String(location!.coordinate.longitude)
        print("User's latitude: \(latitude) and longitude: \(longitude)" )
        self.ref.child("users").child("\(user!.uid)/latitude").setValue(latitude)
        self.ref.child("users").child("\(user!.uid)/longitude").setValue(longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView.setRegion(region, animated: true)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("error:: \(error)")
    }
}

class PinAnnotation:NSObject, MKAnnotation{
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
    class func createViewAnnotationForMap(mapView:MKMapView, annotation:MKAnnotation)->MKAnnotationView{
        if let annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("PinAnnotation"){
            return annotationView
        }else{
            let returnedAnnotationView:MKPinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier:"PinAnnotation")
            returnedAnnotationView.pinTintColor = UIColor.redColor()
            returnedAnnotationView.animatesDrop = true
            returnedAnnotationView.canShowCallout = true
            
            return returnedAnnotationView
            
        }
        
        
    }
}
