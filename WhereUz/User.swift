//
//  User.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/15/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit

class User: NSObject {
    var name: String?
    var email: String?
    var latitude: String?
    var longitude: String?
    var uid: String?
}
