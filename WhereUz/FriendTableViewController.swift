//
//  FriendTableViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/20/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase

class FriendTableViewController: UITableViewController {
    
    let cellId = "cellId"
    
    var users = [User]()
    
    var user = FIRAuth.auth()?.currentUser
    
    var ref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "FriendListTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellId)
        
        fetchFriend()
    }
    
    func fetchFriend() {
        FIRDatabase.database().reference().child("friends").queryOrderedByChild("following").observeEventType(.Value, withBlock: { (snapshot) in
            // atur query firebase friend = true
            if let dictionary = snapshot.key as? [String] {
               // let user = User()

              //  self.users.append(user)
            
                print(dictionary)
                
                
                //print(user.name, user.latitude, user.longitude, user.uid)
            }
            
            }, withCancelBlock: nil)
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId, forIndexPath: indexPath) as! FriendTableViewCell
        
        let user = users[indexPath.row]
        
        cell.userdata = user
        
        return cell
    }
    
}
