//
//  LoginViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/13/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
//import FBSDKLoginKit
//import FBSDKCoreKit

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {
    

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookLoginButton.delegate = self
        
        if let user = FIRAuth.auth()?.currentUser
        {
            self.logoutButton.alpha = 1.0
            self.usernameLabel.text = "Welcome \(user.email!)"
        }
        
        else {
            self.logoutButton.alpha = 0.0
            self.usernameLabel.text = ""
        }
    }
    
    
    @IBAction func registerAccount(sender: AnyObject) {
        if self.emailField.text == "" || self.passwordField.text == "" {
            let alertController = UIAlertController(title: "Oops!", message: "Please enter your email and password.", preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        else {
            FIRAuth.auth()?.createUserWithEmail(self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                if error == nil
                {
                    self.logoutButton.alpha = 1.0
                    self.usernameLabel.text = user!.email
                    self.emailField.text = ""
                    self.passwordField.text = ""
                    
                } else {
                    let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
        }
        
   
    }
    
    @IBAction func loginWithEmailAction(sender: AnyObject) {
        if self.emailField.text == "" || self.passwordField.text == "" {
            let alertController = UIAlertController(title: "Oops!", message: "Please enter your email and password.", preferredStyle: .Alert)
            let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            FIRAuth.auth()?.signInWithEmail(self.emailField.text!, password: self.passwordField.text!, completion: { (user, error) in
                if error == nil
                {
                    self.logoutButton.alpha = 1.0
                    self.usernameLabel.text = user!.email
                    self.emailField.text = ""
                    self.passwordField.text = ""
                    
                } else {
                    let alertController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            })
        }

    }
    
    
    @IBAction func logoutAction(sender: AnyObject) {
        try! FIRAuth.auth()?.signOut()
        
        self.usernameLabel.text = ""
        self.logoutButton.alpha = 0.0
        self.emailField.text = ""
        self.passwordField.text = ""

    }

        // Do any additional setup after loading the view.

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        if error != nil {
            print(error!.localizedDescription)
            return
        }
        
        let credential = FIRFacebookAuthProvider.credentialWithAccessToken(FBSDKAccessToken.currentAccessToken().tokenString)
        FIRAuth.auth()?.signInWithCredential(credential, completion: { (user, error) in
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            print("User logged in with facebook ...")
            })
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        try! FIRAuth.auth()!.signOut()
        print("User logged out from facebook ...")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/13/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

/*import UIKit
import Firebase
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit

class ViewController: UIViewController, FBSDKLoginButtonDelegate {
    
    
    var loginButton: FBSDKLoginButton = FBSDKLoginButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if (FBSDKAccessToken.currentAccessToken() != nil)
        // {
        // User is already logged in, do work such as go to next view controller.
        //}
        //else
        // {
        //let loginButton : FBSDKLoginButton = FBSDKLoginButton()
        self.view!.addSubview(loginButton)
        self.loginButton.center = self.view.center
        self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        self.loginButton.delegate = self;
        // }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        print("User Logged In")
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User did Logout")
    }
}

//
//  LoginViewController.swift
//  WhereUz
//
//  Created by Fitria Ridayanti on 9/13/16.
//  Copyright © 2016 Fitria Ridayanti. All rights reserved.
//

import UIKit
 import FBSDKCoreKit
 import FBSDKLoginKit
 
 
 class ViewController: UIViewController, FBSDKLoginButtonDelegate {
 
 var loginButton: FBSDKLoginButton = FBSDKLoginButton()
 
 override func viewDidLoad() {
 super.viewDidLoad()
 
 // if (FBSDKAccessToken.currentAccessToken() != nil)
 // {
 // User is already logged in, do work such as go to next view controller.
 //}
 //else
 // {
 //let loginButton : FBSDKLoginButton = FBSDKLoginButton()
 self.view!.addSubview(loginButton)
 self.loginButton.center = self.view.center
 self.loginButton.readPermissions = ["public_profile", "email", "user_friends"]
 self.loginButton.delegate = self;
 // }
 
 // Do any additional setup after loading the view, typically from a nib.
 }
 
 override func didReceiveMemoryWarning() {
 super.didReceiveMemoryWarning()
 // Dispose of any resources that can be recreated.
 }
 
 
 func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
 
 print("User Logged In")
 }
 
 func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
 print("User did Logout")
 }
 }*/


